-module(erlogger_anonymous_worker_supervisor).
-behavior(supervisor).
-export([init/1, start_link/0]).

start_link() ->
    supervisor:start_link(?MODULE, []).

init(_) ->
    {ok, {{simple_one_for_one, 5, 10},
          [{log,
            {erlogger_worker, start_link, []},
            temporary,
            10000,
            worker,
            [erlogger_worker]}
          ] 
         }}.
