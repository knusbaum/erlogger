-module(erlogger_worker).
-behavior(gen_server).
-export([start_link/1, start_link/2, init/1, handle_call/3, handle_cast/2,
         handle_info/2, terminate/2, code_change/3, log/3, log_surely/3,
         log_surely/4]).

add_pid(Message) ->
    io_lib:format("~p: ", [self()]) ++ Message.

log({anonymous, Pid}, Message, Args) ->
    gen_server:cast(Pid, {log, add_pid(Message), Args});
log(Name, Message, Args) ->
    gen_server:cast(whereis(Name), {log, add_pid(Message), Args}).

log_surely({anonymous, Pid}, Message, Args) ->
    gen_server:call(Pid, {log, add_pid(Message), Args}, infinity);
log_surely(Name, Message, Args) ->
    gen_server:call(whereis(Name), {log, add_pid(Message), Args}, infinity).

log_surely({anonymous, Pid}, Message, Args, Timeout) ->
    gen_server:call(Pid, {log, add_pid(Message), Args}, Timeout);
log_surely(Name, Message, Args, Timeout) ->
    gen_server:call(whereis(Name), {log, add_pid(Message), Args}, Timeout).

start_link(Logfile) ->
    Res = gen_server:start_link(?MODULE, [Logfile], []),
    error_logger:info_report(["Starting anonymous logger: ", {file, Logfile}, {result, Res}]),
    Res.

start_link(Name, Logfile) ->
    Res = gen_server:start_link({local, Name}, ?MODULE, [Logfile], []),
    error_logger:info_report(["Starting Logger: ", {name, Name}, {file, Logfile}, {result, Res}]),
    Res.

init([Logfile]) ->
    process_flag(trap_exit, true),
    Filename = erlogger_util:make_filename(Logfile),
    case file:open(Filename, [append]) of
        {ok, Dev} -> 
            write_log(Dev, "Starting Logger."),
            {ok, Dev};
        {error, Reason} -> {stop, Reason}
    end.

handle_call({log, Message, Args}, _From, Dev) ->
    write_log(Dev, Message, Args),
    {reply, ok};
handle_call(terminate, _From, Dev) ->
    {stop, normal, ok, Dev}.

handle_cast({log, Message, Args}, Dev) ->
    write_log(Dev, Message, Args),
    {noreply, Dev}.

handle_info({'EXIT', Pid, Reason}, Dev) ->
    write_log(Dev, "Got EXIT from ~p: ~p", [Pid, Reason]),
    {stop, Reason, Dev};
handle_info(Message, Dev) ->
    write_log(Dev, "Unexpected Message: ~p", [Message]),
    {noreply, Dev}.
    
terminate(normal, Dev) ->
    write_log(Dev,"Normal Shutdown."),
    file:close(Dev),
    ok;
terminate(shutdown, Dev) ->
    write_log(Dev,"Got Shutdown from Supervisor."),
    file:close(Dev),
    ok;
terminate(Reason, Dev) ->
    write_log(Dev, "Abnormal Shutdown: ~p.", [Reason]),
    file:close(Dev),
    ok.

code_change(OldVsn, Dev, _Extra) ->
    %% No change planned. The function is here for the behavior,
    %% but will not be used. Only next version.
    write_log(Dev, "Loading new version. Old version: ~p~n", [OldVsn]),
    {ok, Dev}.


%%% Private Functions

write_log(Dev, Message) ->
    write_log(Dev, Message, []).

write_log(Dev, Message, Args) ->
    Date = erlogger_util:make_datestring(),
    Log = io_lib:format(Message, Args),
    file:write(Dev, io_lib:format("~s: ~s~n", [Date, Log])).
