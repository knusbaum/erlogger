-module(erlogger_sup).
-behavior(supervisor).
-export([init/1, start_link/0]).

start_link() ->
    supervisor:start_link(?MODULE, []).

init(_) ->
    {ok, {{one_for_one, 5, 10},
          [{erlogger_serv,
            {erlogger_serv, start_link, [self()]},
            permanent,
            10000,
            worker,
            [erlogger_serv]},
           {erlogger_worker_supervisor,
            {erlogger_worker_supervisor, start_link, []},
            permanent,
            15000,
            supervisor,
            [erlogger_worker_supervisor]},
           {erlogger_anonymous_worker_supervisor,
            {erlogger_anonymous_worker_supervisor, start_link, []},
            permanent,
            15000,
            supervisor,
            [erlogger_anonymous_worker_supervisor]}
          ]
         }}.
