-module(erlogger).
-behavior(application).
-export([start/2,
         start_logger/2,
         start_anonymous/1,
         log/2,
         log/3,
         log_surely/2,
         log_surely/3,
         stop_logger/1,
         stop/1,
         monitor_logger/1]).


start(normal, _Args) ->
    erlogger_sup:start_link().

start_logger(Name, File) ->
    gen_server:call(erlogger, {start_logger, Name, File}).

start_anonymous(File) ->
    gen_server:call(erlogger, {start_anonymous_logger, File}).

monitor_logger({anonymous, Pid}) ->
    monitor(process, Pid);
monitor_logger(Name) ->
    monitor(process, whereis(Name)).

% Logger can be a logger name or anonymous tuple
log(Logger, Message) ->
    log(Logger, Message, []).

log(Logger, Message, Args) ->
    erlogger_worker:log(Logger, Message, Args).

log_surely(Logger, Message) ->
    log_surely(Logger, Message, []).

log_surely(Logger, Message, Args) ->
    erlogger_worker:log_surely(Logger, Message, Args).

stop_logger({anonymous, Pid}) ->
    exit(Pid, shutdown);
stop_logger(Name) ->
    Pid = whereis(Name),
    unregister(Name),
    exit(Pid, shutdown).

stop(_) ->
    gen_server:call(erlogger, shutdown),
    ok.
