-module(erlogger_util).
-export([make_datestring/0, make_timestamp/0, make_filename/1]).

make_datestring() ->
    {{Year, Month, Day},{Hour,Minutes,Seconds}} = erlang:localtime(),
    io_lib:format("[~B,~B,~B ~B:~B:~B]", [Month, Day, Year, Hour, Minutes, Seconds]).

make_timestamp() ->
    {{Year, Month, Day},{Hour,Minutes,Seconds}} = erlang:localtime(),
    io_lib:format("~2..0B~2..0B~B~2..0B~2..0B~2..0B", 
                  [Month, Day, Year, Hour, Minutes, Seconds]).

make_filename(Logfile) ->
    Prefix = case application:get_env(erlogger, logdir) of
                 undefined -> "log/";
                 {ok, Val} -> Val ++ "/"
             end,
    Prefix ++ Logfile.

